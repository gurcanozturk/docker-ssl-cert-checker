#!/bin/env python3

from cryptography import x509
import socket
import ssl
import os
import datetime
import requests
import smtplib

def check_ssl_expire(hostname, port):
  context = ssl.create_default_context()
  context.check_hostname = False
  context.verify_mode = ssl.CERT_NONE

  with socket.create_connection((hostname, port)) as sock:
    with context.wrap_socket(sock, server_hostname=hostname) as ssock:
        data = ssock.getpeercert(True)
        pem_data = ssl.DER_cert_to_PEM_cert(data)
        cert_data = x509.load_pem_x509_certificate(str.encode(pem_data))
        expire = cert_data.not_valid_after.strftime('%s')
        return (expire)

def calc_day_difference(day):
   now = datetime.datetime.now().strftime('%s')
   gun = int(abs(int(now) - int(day))/86400)
   return gun

def check_day_difference(hostname, gun, limit):
  if gun <= limit:
     msg = ("%s SSL certificate about to expire in %s days" % (hostname, gun))
     return msg

def send_gotify_message(gotify_url, message):
   resp = requests.post(gotify_url, json={
     "message": message,
     "priority": 2,
     "title": "SSL certificate about to expire"
   })

def send_mail(smtp_host, smtp_port, sent_from, to, subject, message):
  host = ("%s:%s" % (smtp_host, smtp_port))
  body = "\r\n".join((
    "From: %s" % sent_from,
    "To: %s" % to,
    "Subject: %s" % subject ,
    "",
    message
    ))

  server = smtplib.SMTP_SSL(host)
  server.sendmail(sent_from, [to], body)
  server.quit()

day_count   = int(os.environ['WARNING_DAYS'])
domains     = os.environ['DOMAINS']
gotify_enable = os.environ['GOTIFY_ENABLE']
email_enable  = os.environ['EMAIL_ENABLE']

try:
     domains_split = domains.split(',')
     for domain in domains_split:
        hostname = domain.split(":")[0]
        port = domain.split(":")[1]

        if (hostname):
          gun = calc_day_difference(check_ssl_expire(hostname, port))
          check = (check_day_difference(hostname, gun, day_count))
          if check:
             print (check)
             if gotify_enable   == "true":
                gotify_token    = os.environ['GOTIFY_TOKEN']
                gotify_base_url = os.environ['GOTIFY_URL']
                gotify_url      = ("%s/message?token=%s" % (gotify_base_url, gotify_token))
                send_gotify_message(gotify_url, check)

             if email_enable == "true":
                smtp_host = os.environ['SMTP_HOST']
                smtp_port = os.environ['SMTP_PORT']
                email_from = os.environ['EMAIL_FROM']
                email_to   = os.environ['EMAIL_TO']
                subject = "SSL certificate about to expire"
                send_mail(smtp_host, smtp_port, email_from, email_to, subject, check)
        else:
          print ("no hostname given to check SSL certificate expire date")

except Exception as e:
  print (e)
