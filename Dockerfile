FROM alpine:latest

RUN apk update && apk add py3-cryptography

COPY ./ssl_cert_check.py /

CMD [ "python3", "/ssl_cert_check.py" ]
